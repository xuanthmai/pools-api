from flask import Flask, request
import json
import os
import math

from config import app_config
from response import CustomResponse

app = Flask(__name__)
app.config.from_object(app_config['development'])

def _calc_percentiles(sorted_values, pool_percentile):
    rank = (pool_percentile/100.0)*(len(sorted_values)-1)
    f_rank = math.floor(rank)
    c_rank = math.ceil(rank)

    if f_rank == c_rank: # Rank is an int, percentile in array, no calc needed
        return sorted_values[int(rank)]

    decimal_val = (rank - f_rank) * (sorted_values[c_rank] - sorted_values[f_rank])

    return sorted_values[f_rank] + decimal_val


@app.route('/pools-percentile', methods=['POST'])
def pools_percentile():
    if not request.is_json:
        return CustomResponse.BadRequest('Request is not in json.')
    request_data = request.get_json()

    pool_id = None
    pool_percentile = None

    response = {}

    if ('poolId' in request_data) and ('percentile' in request_data):
        pool_id = request_data['poolId']
        pool_percentile = request_data['percentile']
    else:
        return CustomResponse.BadRequest('JSON does not contain required field.')

    storage_folder_path = os.getcwd() + '/' + app.config['STORAGE']
    storage_file_name = f'{pool_id}.json'

    # Validation
    if not (type(pool_id) is int):
        return CustomResponse.BadRequest('poolId must be a number.')
    if not (os.path.exists(storage_folder_path + storage_file_name)):
        return CustomResponse.NotFound('poolId does not exist.')
    if pool_percentile < 0 or pool_percentile > 100:
        return CustomResponse.BadRequest('percentile must be between 0 and 100.')

    try:
        with open(storage_folder_path + storage_file_name, 'r') as file_object:  
            data = json.load(file_object)

            sorted_values = sorted(data['poolValues'])
            percentile_value = _calc_percentiles(sorted_values, pool_percentile)

            response['calcPercentile'] = round(percentile_value, 4)
            response['totalCount'] = len(sorted_values)
    except:
        return CustomResponse.InternalServerError('Something went wrong. Please try again later.')

    return CustomResponse.Ok(data=response)


@app.route('/pools', methods=['POST'])
def pools():
    if not request.is_json:
        return CustomResponse.BadRequest('Request is not in json.')
    request_data = request.get_json()

    pool_id = None
    pool_values = None

    response = {}

    if ('poolId' in request_data) and ('poolValues' in request_data):
        pool_id = request_data['poolId']
        pool_values = request_data['poolValues']
    else:
        return CustomResponse.BadRequest('JSON does not contain required field.')

    # Validation
    if not (type(pool_id) is int):
        return CustomResponse.BadRequest('poolId must be a number.')
    if not (type(request_data['poolValues']) is list):
        return CustomResponse.BadRequest('poolValues must be an array.')
    if not (len(request_data['poolValues']) > 0):
        return CustomResponse.BadRequest('poolValues must include at least 1 value.')
                
    storage_folder_path = os.getcwd() + '/' + app.config['STORAGE']
    try:
        # Create storage folder
        if not (os.path.exists(storage_folder_path)):
            os.makedirs(storage_folder_path) 
        
        storage_file_name = f'{pool_id}.json'

        for _, _, files in os.walk(storage_folder_path):
            # Existing pool id, so open existing storage file to append
            if storage_file_name in files: 
                with open(storage_folder_path + storage_file_name, 'r') as file_object:  
                    data = json.load(file_object)
                    data['poolValues'] += pool_values

                    response['status'] = 'Appended'
            else: # New pool id
                data = request_data

                response['status'] = 'Inserted'
                
        # Store pool id and its value to a json file as storage
        with open(storage_folder_path + storage_file_name, 'w') as file_object:
            json.dump(data, file_object)
    except:
        # Most likely issue with reading/writing to file storage
        return CustomResponse.InternalServerError('Something went wrong. Please try again later.')

    return CustomResponse.Ok(data=response)


if __name__ == '__main__':
    # debug mode
    app.run(host='0.0.0.0', debug=True, port=6060)
