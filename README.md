# Setup
Install `python 3.8.x`  
Install packages through pip: `pip install -r requirements.txt`  

# Usage
* To start service: `python pools.py`
* To run tests: `python test_pools.py`
* To change storage config:  Edit `config.py`