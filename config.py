import os


class Config(object):
    """Parent configuration class."""
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True


class TestConfig(Config):
    TESTING = True
    DEBUG = True
    STORAGE = 'storage/test/'


class DevelopmentConfig(Config):
    STORAGE = 'storage/dev/'


app_config = {
    'test': TestConfig,
    'development': DevelopmentConfig,
}