from flask import Flask
import json

app = Flask(__name__)

class CustomResponse:
    @staticmethod
    def Ok(data=None):
        response = app.response_class(
            response=json.dumps({
                'success': True,
                'data': data
            }),
            status=200,
            mimetype='application/json'
        )
        return response


    @staticmethod
    def BadRequest(message='Bad Request'):
        response = app.response_class(
            response=json.dumps({
                'success': False,
                'data': {
                    'error': True,
                    'message': message
                }
            }),
            status=400,
            mimetype='application/json'
        )
        return response

    @staticmethod
    def NotFound(message='Not Found'):
        response = app.response_class(
            response=json.dumps({
                'success': False,
                'data': {
                    'error': True,
                    'message': message
                }
            }),
            status=404,
            mimetype='application/json'
        )
        return response

    @staticmethod
    def InternalServerError(message='Internal Server Error'):
        response = app.response_class(
            response=json.dumps({
                'success': False,
                'data': {
                    'error': True,
                    'message': message
                }
            }),
            status=500,
            mimetype='application/json'
        )
        return response
