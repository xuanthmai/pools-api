import unittest
import os
import json
import shutil

from pools import app
from config import app_config

class PoolsTestCase(unittest.TestCase):
    def setUp(self):
        self.app = app
        self.app.config.from_object(app_config['test'])

        # Clear test storage if exists
        if (os.path.exists(self.app.config['STORAGE'])):
            shutil.rmtree(self.app.config['STORAGE'])

    ''' Test valid requests'''
    def test_pools_insert(self):
        response = app.test_client().post(
            '/pools',
            data=json.dumps({'poolId': 1, 'poolValues': [1,2,3,4]}),
            content_type='application/json'
        )

        data = json.loads(response.get_data(as_text=True))

        assert response.content_type == 'application/json'
        assert response.status_code == 200
        assert data['success'] == True
        assert data['data']['status'] == 'Inserted'

        # Check content of storage
        with open(self.app.config['STORAGE'] + '1.json','r') as f:
            output = f.read()
        assert output == '{"poolId": 1, "poolValues": [1, 2, 3, 4]}'


    def test_pools_append(self):
        app.test_client().post(
            '/pools',
            data=json.dumps({'poolId': 2, 'poolValues': [1,2,3,4]}),
            content_type='application/json'
        )

        # Add more value to existing pool
        response = app.test_client().post(
            '/pools',
            data=json.dumps({'poolId': 2, 'poolValues': [5,6]}),
            content_type='application/json'
        )

        data = json.loads(response.get_data(as_text=True))

        assert response.content_type == 'application/json'
        assert response.status_code == 200
        assert data['success'] == True
        assert data['data']['status'] == 'Appended'

        # Check content of storage
        with open(self.app.config['STORAGE'] + '2.json','r') as f:
            output = f.read()
        assert output == '{"poolId": 2, "poolValues": [1, 2, 3, 4, 5, 6]}'

    
    def test_pools_percentile_no_calc(self):
        # Insert sample data
        app.test_client().post(
            '/pools',
            data=json.dumps({'poolId': 3, 'poolValues': [1,2,3,4,5]}),
            content_type='application/json'
        )

        response = app.test_client().post(
            '/pools-percentile',
            data=json.dumps({'poolId': 3, 'percentile': 25}),
            content_type='application/json'
        )
        data = json.loads(response.get_data(as_text=True))
        
        assert response.content_type == 'application/json'
        assert response.status_code == 200
        assert data['success'] == True
        assert data['data']['calcPercentile'] == 2
        assert data['data']['totalCount'] == 5


    def test_pools_percentile_calc_needed(self):
        # Insert sample data
        app.test_client().post(
            '/pools',
            data=json.dumps({
                'poolId': 4,
                'poolValues': [
                    340, 225, 942, 303, 566, 893, 610, 658, 446, 
                    200, 519, 438, 872, 531, 730, 264, 131, 464
                ]}),
            content_type='application/json'
        )

        response = app.test_client().post(
            '/pools-percentile',
            data=json.dumps({'poolId': 4, 'percentile': 95.8}),
            content_type='application/json'
        )
        data = json.loads(response.get_data(as_text=True))
        
        assert response.content_type == 'application/json'
        assert response.status_code == 200
        assert data['success'] == True
        assert data['data']['calcPercentile'] == 907.014
        assert data['data']['totalCount'] == 18


    def test_pools_percentile_calc_needed_long(self):    
        # Insert sample data
        app.test_client().post(
            '/pools',
            data=json.dumps({
                'poolId': 5,
                'poolValues': [
                    918, 268, 754, 515, 339, 38, 801, 434, 677, 707, 814, 908, 878, 155, 
                    487, 115, 151, 490, 697, 809, 100, 735, 159, 397, 598, 861, 709, 87, 
                    500, 130, 160, 867, 829, 537, 917, 381, 378, 9
                ]}),
            content_type='application/json'
        )

        response = app.test_client().post(
            '/pools-percentile',
            data=json.dumps({'poolId': 5, 'percentile': 6.28951}),
            content_type='application/json'
        )
        data = json.loads(response.get_data(as_text=True))
        
        assert response.content_type == 'application/json'
        assert response.status_code == 200
        assert data['success'] == True
        assert data['data']['calcPercentile'] == 91.2525
        assert data['data']['totalCount'] == 38


    ''' Test invalid requests'''
    def test_pools_insert_invalid_poolId(self):
        response = app.test_client().post(
            '/pools',
            data=json.dumps({'poolId': 'a', 'poolValues': [1,2,3,4]}),
            content_type='application/json'
        )
        data = json.loads(response.get_data(as_text=True))

        assert response.content_type == 'application/json'
        assert response.status_code == 400
        assert data['success'] == False
        assert data['data']['error'] == True
        assert data['data']['message'] == 'poolId must be a number.'
        assert os.path.exists(self.app.config['STORAGE'] + 'a.json') == False


    def test_pools_insert_empty_poolValues(self):
        response = app.test_client().post(
            '/pools',
            data=json.dumps({'poolId': 1, 'poolValues': []}),
            content_type='application/json'
        )
        data = json.loads(response.get_data(as_text=True))

        assert response.content_type == 'application/json'
        assert response.status_code == 400
        assert data['success'] == False
        assert data['data']['error'] == True
        assert data['data']['message'] == 'poolValues must include at least 1 value.'
        assert os.path.exists(self.app.config['STORAGE'] + '1.json') == False


    def test_pools_percentile_calc_invalid_percentile(self):
        app.test_client().post(
            '/pools',
            data=json.dumps({
                'poolId': 1,
                'poolValues': [
                    1, 3, 4
                ]}),
            content_type='application/json'
        )

        response = app.test_client().post(
            '/pools-percentile',
            data=json.dumps({'poolId': 1, 'percentile': 120.95}),
            content_type='application/json'
        )
        data = json.loads(response.get_data(as_text=True))

        assert response.content_type == 'application/json'
        assert response.status_code == 400
        assert data['success'] == False
        assert data['data']['error'] == True
        assert data['data']['message'] == 'percentile must be between 0 and 100.'


    def tearDown(self):
        # Clear test storage after finishing
        if (os.path.exists(self.app.config['STORAGE'])):
            shutil.rmtree(self.app.config['STORAGE'])


if __name__ == "__main__":
    unittest.main()
